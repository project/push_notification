What is this module?
--------------------

This module faciliates to send push notificaiton to iOS and Android users
from drupal backend. This is very useful for Decoupled Drupal implementation.
The module uses firebase push notification.

How it works?
-----------------
Pre-requisite
a. Goto google console and create firebase server key from below url
URL:https://console.firebase.google.com/u/0/.
b. Set iOS and Android certificates here.


Configurations:
---------------

Please follow the instructions below to set server key and firebase URL
via admin panel.

1) Click @site_url/admin/config/push-notification_settings
2) You will find input to enter Server Key in Configuration->Push Notification.
(Set the key you received from google firebase console)
3) Save the configuration.

Push Notification Form:
-----------------------

Please follow the instructions below to send push notification
to all of your users based on Role.

1) open @site_url/push-notification to open the push notification form.
2) You may choose Roles from select roles if you want to send
notification to particular roles.
3) Type title and message.
4) Submit


Device Token:
-------------

Please note that, when you enable this module
"Device Token (field_device_token)" field will be
generated on User Account
(You can find it here: @site_url/admin/config/people/accounts/fields).

You need to insert the device token in this field
otherwise user will not get the push notification.

Note: if you do not store device token in drupal then
Push Notification will not work.

Requirement:
------------

You need to Install and Configure Chosen Module with it's Libraries
to get good Experience of this push notification form.
