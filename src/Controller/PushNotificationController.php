<?php

namespace Drupal\push_notification\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Push Notification controller.
 */
class PushNotificationController extends ControllerBase {

  /**
   * Returns a render-able array for a stats page.
   */
  public static function sendPushNotifications($device_token, $push_notify_title, $push_notify_message) {

    $url = "https://fcm.googleapis.com/fcm/send";
    $token = $device_token;
    $serverKey = \Drupal::config('push_notification.settings')->get('serverKey');
    $title = $push_notify_title;
    $body = $push_notify_message;
    $notification = [
      'title' => $title,
      'text' => $body,
      'sound' => 'default',
      'badge' => '1',
    ];
    $arrayToSend = [
      'to' => $token,
      'notification' => $notification,
      'priority' => 'high',
    ];
    $json = json_encode($arrayToSend);
    $headers = [];
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=' . $serverKey;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Send the request.
    $response = curl_exec($ch);

    // Close request.
    if ($response === TRUE) {
      curl_close($ch);
    }
  }

}
