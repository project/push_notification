<?php

namespace Drupal\push_notification\Form;

/**
 * @file
 * Provides push_notification functionality.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements configuration form for Push Notification.
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class PushNotificationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'push_notification.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'push_notification_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('push_notification.settings');

    $form['serverKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server Key'),
      '#maxlength' => 250,
      '#required' => TRUE,
      '#default_value' => $config->get('serverKey'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $this->config('push_notification.settings')
      ->set('serverKey', $form_state->getValue('serverKey'))
      ->save();
  }

}
