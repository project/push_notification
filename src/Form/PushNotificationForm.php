<?php

namespace Drupal\push_notification\Form;

/**
 * @file
 * Provides push_notification functionality.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\push_notification\Controller\PushNotificationController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements the youtube subscriber button form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class PushNotificationForm extends FormBase {

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Call Constructor.
   */
  public function __construct(QueryFactory $entityQuery, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityQuery = $entityQuery;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Static Create Method.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'push_notification_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $roles_obj = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $user_roles = array_combine(array_keys($roles_obj), array_map(function ($a) {
      return $a->label();
    }, $roles_obj));
    unset($user_roles['anonymous']);

    $form['user_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Select User Roles'),
      '#required' => FALSE,
      '#options' => $user_roles,
      '#size' => 10,
      '#weight' => 0,
      '#attributes' => [
        'id' => 'push_notification_user_roles',
        'class' => ['chosen-select'],
      ],
    ];

    $form['push_notification_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
    ];

    $form['push_notification_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Push Notify'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Stores Submitted Values into variables.
    $push_notify_title = $form_state->getValue('push_notification_title');
    $push_notify_message = $form_state->getValue('push_notification_message');
    $user_roles = $form_state->getValue('user_roles');

    $ids = '';
    if (!empty($user_roles)) {
      $ids = $this->entityQuery->get('user')
        ->condition('status', 1)
        ->condition('roles', $user_roles, 'IN')
        ->execute();
    }
    else {
      $ids = $this->entityQuery->get('user')
        ->condition('status', 1)
        ->execute();
    }

    $users = $this->entityTypeManager->getStorage('user')->loadMultiple($ids);

    foreach ($users as $user_array) {
      $device_token = trim($user_array->get('field_device_token')->value);

      // This will sent Unlimited Notifications to Customers via Admin.
      $send_notification = PushNotificationController::sendPushNotifications($device_token, $push_notify_title, $push_notify_message);

      if ($send_notification) {
        $this->messenger()->addStatus($this->t('Push Notifications has been sent successfully to the Users!'), TRUE);
      }
      else {
        $this->messenger()->addError($this->t('Error while sending Push Notifications! Please try again Later!'), TRUE);
      }

      return new RedirectResponse(Url::fromRoute('push_notification.push_notify_form')->toString());
    }
  }

}
